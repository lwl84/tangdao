package com.tangdao.module.core.service;

import com.tangdao.module.core.entity.Group;
import com.tangdao.framework.service.ICrudService;

/**
 * <p>
 * 用户组表 服务类
 * </p>
 *
 * @author ruyangit@gmail.com
 * @since 2020-03-11
 */
public interface IGroupService extends ICrudService<Group> {

}
