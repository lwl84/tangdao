package com.tangdao.module.core.service;

import com.tangdao.module.core.entity.Role;
import com.tangdao.framework.service.ICrudService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ruyangit@gmail.com
 * @since 2020-02-26
 */
public interface IRoleService extends ICrudService<Role> {

}
