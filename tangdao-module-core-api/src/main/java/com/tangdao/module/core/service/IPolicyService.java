package com.tangdao.module.core.service;

import com.tangdao.module.core.entity.Policy;
import com.tangdao.framework.service.ICrudService;

/**
 * <p>
 * 策略表 服务类
 * </p>
 *
 * @author ruyangit@gmail.com
 * @since 2020-02-26
 */
public interface IPolicyService extends ICrudService<Policy> {

}
