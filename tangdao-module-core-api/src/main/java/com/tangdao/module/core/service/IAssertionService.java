package com.tangdao.module.core.service;

import com.tangdao.module.core.entity.Assertion;
import com.tangdao.framework.service.ICrudService;

/**
 * <p>
 * 断言表 服务类
 * </p>
 *
 * @author ruyangit@gmail.com
 * @since 2020-02-26
 */
public interface IAssertionService extends ICrudService<Assertion> {

}
