package com.tangdao.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TangdaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TangdaoApplication.class, args);
	}
}
